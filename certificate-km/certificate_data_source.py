import os
import logging as log
from urllib.parse import urlparse
import hashlib
import requests
from requests.auth import HTTPBasicAuth

from knowledge_mapper.data_source import DataSource


GLOBAL_GAP = 'https://www.tno.nl/agrifood/ontology/ploutos/sip1#globalGap'
QMS_CERT = 'https://www.tno.nl/agrifood/ontology/ploutos/sip1#qmsCert'

class CertificateDataSource(DataSource):
    def __init__(self, url):
        self.url = urlparse(url)

        # Read the username and password from environment variables. You should
        # set these in `.env` in the docker-compose project. (Or pass them in
        # another way in the live version.)
        if 'CERTIFICATE_USERNAME' in os.environ and 'CERTIFICATE_PASSWORD' in os.environ:
            self.username = os.environ['CERTIFICATE_USERNAME']
            self.password = os.environ['CERTIFICATE_PASSWORD']


    def test(self):
        response = requests.get(
            f'{self.url.geturl()}',
            auth=HTTPBasicAuth(self.username, self.password),
        )
        if not response.ok:
            raise Exception(f'Unexpected status while testing {self.url.geturl()}: {response.status_code}')
        else:
            log.info(f'Successfully tested {self.url.geturl()}.')


    def handle(self, ki, binding_set, requesting_kb):
        """Handle the `binding_set` with incoming bindings, for the knowledge interaction `ki`."""
        log.info(f'incoming: {binding_set}')

        if ki['name'] == 'certificates':
            result = self.handle_certificates(ki, binding_set)
        elif ki['name'] == 'receive-farming-calendar':
            result = self.handle_incoming_farming_calendar(ki, binding_set)
        else:
            log.error(f'unknown ki {ki["name"]}')

        log.info(f'outgoing: {result}')

        return result


    def handle_certificates(self, ki, binding_set):
        results = []
        for incoming_binding in binding_set:
            if 'asoiId' in incoming_binding and 'asoiProvider' in incoming_binding:
                asoi_id = incoming_binding["asoiId"]
                asoi_provider = incoming_binding["asoiProvider"]
                if asoi_provider == f'<{GLOBAL_GAP}>':
                    url = f'{self.url.geturl()}/certificate/{asoi_id[1:-1]}'
                    response = requests.get(url, auth=HTTPBasicAuth(self.username, self.password),)
                    if response.ok:
                        body = response.json()
                        for cert in body['certifications']:
                            asci_provider = self.map_certifier(body["certified_by"]) # TODO: This assumes that the certifier provided the certificate identifier.
                            if asci_provider is not None:
                                results.append({
                                    'certificate': f'<https://www.tno.nl/agrifood/data/ploutos/sip1#{cert["id"]}>',
                                    'certificateSource': f'<{body["certification_file"]}>',
                                    'asci': f'<{self.make_hashed_iri("ascis", [cert["id"], asci_provider])}>',
                                    'asciId': f'"{cert["id"]}"',
                                    'asciProvider': f'<{asci_provider}>',
                                    'organisation': f'<https://www.tno.nl/agrifood/data/ploutos/sip1#{"-".join(body["organization"].split())}>',
                                    'asoi': f'<{self.make_hashed_iri("asois", [asoi_id[1:-1], asoi_provider[1:-1]])}>',
                                    'asoiId': asoi_id,
                                    'asoiProvider': asoi_provider,
                                    'scheme': f'"{body["scheme"]}"',
                                    'validFrom': f'"{body["valid_from"]}"^^xsd:date',
                                    'validUntil': f'"{body["valid_to"]}"^^xsd:date',
                                    'productClass': f'<https://www.tno.nl/agrifood/data/ploutos/sip1#{"-".join(cert["name"].split())}>',
                                })
                else:
                    log.warn(f'unsupported authority-specific organisation identifier provider {asoi_provider}')
            else:
                log.warn('expecting authority-specific organisation identifier (with id and provider), returning no bindings.')
        return results


    def handle_incoming_farming_calendar(self, ki, binding_set):
        result_bindings = []
        # store the farming calendar somewhere in hte knowledge base and
        # provide a new certification ID back to the requesting knowledge base
        new_result_binding = {
            'certificate': '<https://www.tno.nl/agrifood/data/ploutos/sip1#cert-instance-alterra>',
            'organisation':'<https://www.tno.nl/agrifood/data/ploutos/sip1#AlterraFruitProcessor>',
        }
        result_bindings.append(new_result_binding)
        
        return result_bindings

    def make_hashed_iri(self, namespace, parts, joiner='-'):
        should_be_unique = joiner.join(parts)
        hashed = hashlib.md5(should_be_unique.encode("utf-8")).hexdigest()
        return f'{self.url.scheme}://{self.url.netloc}/{namespace}/{hashed}'

    def map_certifier(self, certifier_string):
        if certifier_string == 'QMSCert':
            return QMS_CERT
        else:
            log.warn(f'unknown certifier {certifier_string}; mapping should be added!')
            return None
