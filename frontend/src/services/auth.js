import { f7 } from "framework7-vue";
import store from "../js/store";

// configurable during build by setting env variable
const backend_endpoint = import.meta.env.VITE_PLOUTOS_BACKEND ?? "https://back.ploutos-docker.hispatecanalytics.com"

export default function Login(payload) {
    fetch(`${backend_endpoint}/api/v1/login/`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(payload),
    }).then(response => {
        return response.json();
      }).then(data => {
        if (data['token']) {
          store.state.token = data['token'];
          store.state.user = data;
          f7.views.main.router.navigate('/home/', { reloadCurrent: true });
        } else {
          f7.views.main.router.navigate('/login/', { reloadCurrent: true });
          f7.dialog.alert("Invalid credentials");
        }
      }).catch(err => {
        console.log(err)
      });
}
