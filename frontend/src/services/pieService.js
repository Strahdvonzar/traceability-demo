import { f7 } from "framework7-vue";
import store from "../js/store";

// configurable during build by setting env variable
const backend_endpoint = import.meta.env.VITE_PLOUTOS_BACKEND ?? "https://back.ploutos-docker.hispatecanalytics.com"

export default function getLot(id) {
    //function to just get the outputs of the desired lot
    fetch(`${backend_endpoint}/api/v1/pie/${id}/get_lot/`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': `Token ${store.state.token}`
        },
    }).then(response => {
        return response.json();
      }).then(lotInfo => {
        if (lotInfo.length > 0) {
          store.state.lot = lotInfo;
          f7.preloader.hide();
          f7.views.main.router.navigate('/lot/', { reloadCurrent: true });
        } else {
          f7.views.main.router.navigate('/home/', { reloadCurrent: true });
          f7.preloader.hide();
          f7.dialog.alert("Wrong lot id");
        }
      }).catch(err => {
        console.log(err)
      });
}
